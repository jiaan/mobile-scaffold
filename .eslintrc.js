module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-unused-vars': ['error', {
      varsIgnorePattern: '^h$',
      argsIgnorePattern: '^h$',
    },
    ],
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
};
